@regression

Feature: Spotify API tests

  @userProfile
  Scenario Outline: Validate user profile API with "<userID>" and "<dataType>" credentials
    Given that we retrieve <userID> with <dataType> credentials
    When the status code equals <statusCode>
    Then the display name equals <displayName>
    Examples:
      | userID                    | dataType | statusCode | displayName            |
      | 45nrfy9ayyanv89rfjrao3al2 | valid    | 200        | "Testing123"           |
      | 3hlhj0w7sjnk4yzjb3ixkpanw | valid    | 200        | "ticalag"              |
      | ticalag                   | valid    | 404        | "No such user"         |
      | user profile              | invalid  | 401        | "Invalid access token" |

  @followHappyPath
  Scenario: Validate Follow API happy flow
    Given that we retrieve the list of followed playlists
    And the user shouldn't follow the expected playlist
    When we follow a valid playlist
    Then we should have 1 new playlist added to the list
    And the user should follow the expected playlist
    When we unfollow a valid playlist
    Then we should have 0 new playlist added to the list
    And the user shouldn't follow the expected playlist


  @followNegativePath
  Scenario: Validate Follow API negative flow
    Given that we retrieve the list of followed playlists
    And the user shouldn't follow the expected playlist
    When we follow a invalid playlist
    And we should have 0 new playlist added to the list
    And the user shouldn't follow the expected playlist
    Then we unfollow a invalid playlist

  @library
  Scenario Outline: Validate Library API flow for <libraryType>
    Given that we retrieve the list of saved <libraryType>
    And we save a valid <libraryType>
    And we should have 1 new <libraryType> added
    And the current user should contain saved <libraryType>
    And we remove a valid <libraryType>
    And we should have 0 new <libraryType> added
    And the current user shouldn't contain saved <libraryType>
    Examples:
      | libraryType |
      | album       |
      | episode     |
      | track       |








