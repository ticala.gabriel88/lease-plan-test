package com.lease.plan.cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import static com.lease.plan.api.SpotifyApiRequests.deleteAlbum;
import static com.lease.plan.api.SpotifyApiRequests.deleteEpisode;
import static com.lease.plan.api.SpotifyApiRequests.deleteTrack;
import static com.lease.plan.api.SpotifyApiRequests.doesLibraryTypeContainID;
import static com.lease.plan.api.SpotifyApiRequests.followPlaylistID;
import static com.lease.plan.api.SpotifyApiRequests.isUserIdFollowingPlaylistID;
import static com.lease.plan.api.SpotifyApiRequests.requestNumberOfCurrentUsersPlaylists;
import static com.lease.plan.api.SpotifyApiRequests.requestNumberOfCurrentUsersSavedAlbums;
import static com.lease.plan.api.SpotifyApiRequests.requestNumberOfCurrentUsersSavedEpisodes;
import static com.lease.plan.api.SpotifyApiRequests.requestNumberOfCurrentUsersSavedTracks;
import static com.lease.plan.api.SpotifyApiRequests.saveAlbum;
import static com.lease.plan.api.SpotifyApiRequests.saveEpisode;
import static com.lease.plan.api.SpotifyApiRequests.saveTrack;
import static com.lease.plan.api.SpotifyApiRequests.unfollowPlaylistID;
import static com.lease.plan.cucumber.steps.CucumberHooks.getAccessToken;
import static com.lease.plan.utils.TestConstants.CURRENT_USER_ID;
import static com.lease.plan.utils.TestConstants.INVALID_PLAYLIST_ID;
import static com.lease.plan.utils.TestConstants.INVALID_TOKEN;
import static com.lease.plan.utils.TestConstants.JOE_ROGAN_MAT_FRASER_EPISODE;
import static com.lease.plan.utils.TestConstants.THE_WEEKND_AFTER_HOURS_ALBUM;
import static com.lease.plan.utils.TestConstants.THE_WEEKND_ALONE_AGAIN_TRACK;
import static com.lease.plan.utils.TestConstants.TOP_50_GLOBAL_PLAYLIST_ID;
import static com.lease.plan.utils.TestConstants.USER_PROFILE_ENDPOINT;
import static com.lease.plan.utils.TestConstants.VALID;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StepDefinitions {


    public static Response response;
    private Integer initialNumberOfCurrentUsersPlaylists;
    private Integer initialNumberOfCurrentUsersSavedAlbums;
    private Integer initialNumberOfCurrentUsersSavedEpisodes;
    private Integer initialNumberOfCurrentUsersSavedTracks;

    @Given("that we retrieve {} with {word} credentials")
    public void thatWeRetrieveUserProfileWith(String userID, String dataType) {
        response = given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(dataType.equals(VALID) ? getAccessToken() : INVALID_TOKEN)
                .get(USER_PROFILE_ENDPOINT + userID);

    }

    @When("the status code equals {int}")
    public void theStatusCodeEquals(int statusCode) {
        assertEquals(statusCode, response.statusCode());

    }

    @Then("the display name equals {string}")
    public void theDisplayNameEquals(String displayName) {
        if (displayName.equals("No such user") || displayName.equals("Invalid access token")) {
            assertEquals(displayName, response.jsonPath().getMap("error").get("message"));
        } else {
            assertEquals(displayName, response.getBody().path("display_name"));

        }

    }

    @Given("that we retrieve the list of followed playlists")
    public void thatWeRetrieveTheListOfFollowedPlaylists() {
        setInitialNumberOfCurrentUsersPlaylists(requestNumberOfCurrentUsersPlaylists());
    }


    @Then("we should have {int} new playlist added to the list")
    public void weShouldHaveNewPlaylistAddedToTheList(int numberOfPlaylists) {
        assertEquals(getInitialNumberOfCurrentUsersPlaylists() + numberOfPlaylists, requestNumberOfCurrentUsersPlaylists());
    }

    @When("^we (follow|unfollow) a (valid|invalid) playlist$")
    public void weFollowAPlaylist(String followType, String dataType) {
        if ("follow".equals(followType)) {
            followPlaylistByDataType(dataType);
        } else if ("unfollow".equals(followType)) {
            unfollowPlaylistByDataType(dataType);
        }
    }

    private void unfollowPlaylistByDataType(String dataType) {
        if (VALID.equals(dataType)) {
            unfollowPlaylistID(TOP_50_GLOBAL_PLAYLIST_ID, dataType);
        } else {
            unfollowPlaylistID(INVALID_PLAYLIST_ID, dataType);
        }
    }

    private void followPlaylistByDataType(String dataType) {
        if (VALID.equals(dataType)) {
            followPlaylistID(TOP_50_GLOBAL_PLAYLIST_ID, dataType);
        } else {
            followPlaylistID(INVALID_PLAYLIST_ID, dataType);
        }
    }


    @And("^the user (should|shouldn't) follow the expected playlist$")
    public void theUserFollowsTheExpectedPlaylist(String followType) {
        switch (followType) {
            case "should":
                assertTrue(isUserIdFollowingPlaylistID(TOP_50_GLOBAL_PLAYLIST_ID, CURRENT_USER_ID));
                break;
            case "shouldn't":
                assertFalse(isUserIdFollowingPlaylistID(TOP_50_GLOBAL_PLAYLIST_ID, CURRENT_USER_ID));
                break;
            default:
                Assert.fail("Invalid followType: " + followType);
        }

    }

    @Given("^that we retrieve the list of saved (episode|album|track)$")
    public void thatWeRetrieveTheListOfSavedEpisodes(String listType) {
        switch (listType) {
            case "album":
                setInitialNumberOfCurrentUsersSavedAlbums(requestNumberOfCurrentUsersSavedAlbums());
                break;
            case "episode":
                setInitialNumberOfCurrentUsersSavedEpisodes(requestNumberOfCurrentUsersSavedEpisodes());
                break;
            case "track":
                setInitialNumberOfCurrentUsersSavedTracks(requestNumberOfCurrentUsersSavedTracks());
                break;
            default:
                Assert.fail("Invalid listType: " + listType);
        }
    }


    @And("^we (save|remove) a (valid|invalid) (episode|album|track)")
    public void weSaveAValidAlbum(String action, String dataType, String libraryType) {
        if ("save".equals(action)) {
            saveSelectedLibraryType(dataType, libraryType);
        } else {
            deleteSelectedLibraryType(dataType, libraryType);
        }
    }

    private void deleteSelectedLibraryType(String dataType, String libraryType) {
        if ("episode".equals(libraryType)) {
            deleteEpisode(dataType, JOE_ROGAN_MAT_FRASER_EPISODE);
        } else if ("album".equals(libraryType)) {
            deleteAlbum(dataType, THE_WEEKND_AFTER_HOURS_ALBUM);
        } else if ("track".equals(libraryType)) {
            deleteTrack(dataType, THE_WEEKND_ALONE_AGAIN_TRACK);
        } else {
            Assert.fail("Invalid libraryType " + libraryType);
        }
    }

    private void saveSelectedLibraryType(String dataType, String libraryType) {
        if ("episode".equals(libraryType)) {
            saveEpisode(dataType, JOE_ROGAN_MAT_FRASER_EPISODE);
        } else if ("album".equals(libraryType)) {
            saveAlbum(dataType, THE_WEEKND_AFTER_HOURS_ALBUM);
        } else if ("track".equals(libraryType)) {
            saveTrack(dataType, THE_WEEKND_ALONE_AGAIN_TRACK);
        } else {
            Assert.fail("Invalid libraryType " + libraryType);
        }
    }


    @And("we should have {int} new {word} added")
    public void weShouldHaveNewAdded(int numberOfNewItemsInTheList, String libraryType) {
        if ("album".equals(libraryType)) {
            assertEquals(getInitialNumberOfCurrentUsersSavedAlbums() + numberOfNewItemsInTheList, requestNumberOfCurrentUsersSavedAlbums());
        } else if ("episode".equals(libraryType)) {
            assertEquals(getInitialNumberOfCurrentUsersSavedEpisodes() + numberOfNewItemsInTheList, requestNumberOfCurrentUsersSavedEpisodes());
        } else if ("track".equals(libraryType)) {
            assertEquals(getInitialNumberOfCurrentUsersSavedTracks() + numberOfNewItemsInTheList, requestNumberOfCurrentUsersSavedTracks());
        } else {
            Assert.fail("Invalid libraryType: " + libraryType);
        }
    }

    @And("^the current user (should|shouldn't) contain saved (episode|album|track)$")
    public void theCurrentUserShouldContainSavedAlbum(String contain, String libraryType) {
        if ("episode".equals(libraryType)) {
            validateThatSelectedLibraryContainsSelectedID(contain, libraryType, JOE_ROGAN_MAT_FRASER_EPISODE);
        } else if ("album".equals(libraryType)) {
            validateThatSelectedLibraryContainsSelectedID(contain, libraryType, THE_WEEKND_AFTER_HOURS_ALBUM);
        } else if ("track".equals(libraryType)) {
            validateThatSelectedLibraryContainsSelectedID(contain, libraryType, THE_WEEKND_ALONE_AGAIN_TRACK);
        } else {
            Assert.fail("Invalid libraryType: " + libraryType);
        }
    }

    private void validateThatSelectedLibraryContainsSelectedID(String contain, String libraryType, String idToContain) {
        switch (contain) {
            case "should":
                assertTrue(doesLibraryTypeContainID(libraryType, idToContain));
                break;
            case "shouldn't":
                assertFalse(doesLibraryTypeContainID(libraryType, idToContain));
                break;
            default:
                Assert.fail("Invalid contain type: " + contain);
                break;
        }
    }


    public int getInitialNumberOfCurrentUsersPlaylists() {
        return initialNumberOfCurrentUsersPlaylists;
    }

    public void setInitialNumberOfCurrentUsersPlaylists(Integer initialNumberOfCurrentUsersPlaylists) {
        this.initialNumberOfCurrentUsersPlaylists = initialNumberOfCurrentUsersPlaylists;
    }

    public Integer getInitialNumberOfCurrentUsersSavedAlbums() {
        return initialNumberOfCurrentUsersSavedAlbums;
    }

    public void setInitialNumberOfCurrentUsersSavedAlbums(Integer initialNumberOfCurrentUsersSavedAlbums) {
        this.initialNumberOfCurrentUsersSavedAlbums = initialNumberOfCurrentUsersSavedAlbums;
    }

    public Integer getInitialNumberOfCurrentUsersSavedEpisodes() {
        return initialNumberOfCurrentUsersSavedEpisodes;
    }

    public void setInitialNumberOfCurrentUsersSavedEpisodes(Integer initialNumberOfCurrentUsersSavedEpisodes) {
        this.initialNumberOfCurrentUsersSavedEpisodes = initialNumberOfCurrentUsersSavedEpisodes;
    }

    public Integer getInitialNumberOfCurrentUsersSavedTracks() {
        return initialNumberOfCurrentUsersSavedTracks;
    }

    public void setInitialNumberOfCurrentUsersSavedTracks(Integer initialNumberOfCurrentUsersSavedTracks) {
        this.initialNumberOfCurrentUsersSavedTracks = initialNumberOfCurrentUsersSavedTracks;
    }


}
