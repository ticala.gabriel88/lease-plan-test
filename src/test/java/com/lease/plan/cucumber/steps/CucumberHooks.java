package com.lease.plan.cucumber.steps;


import io.cucumber.java.Before;

import static com.lease.plan.utils.TestConstants.AUTHORIZATION_HEADER;
import static com.lease.plan.utils.TestConstants.REFRESH_TOKEN;
import static io.restassured.RestAssured.given;

public class CucumberHooks {

    private static String accessToken;

    public static String getAccessToken() {
        return accessToken;
    }

    public static void setAccessToken(String accessToken) {
        CucumberHooks.accessToken = accessToken;
    }


    @Before()
    public void setUp() {
        generateAccessToken();
    }

    /**
     * Spotify's API uses oauth2 which requires an access_token which expires after 1 hour
     * because of this, we need to make this request to refresh the token and get a new access_token.
     * More details about this process can be found on:
     * https://developer.spotify.com/documentation/general/guides/authorization-guide/
     */
    private void generateAccessToken() {
        setAccessToken(given()
                .param("grant_type", "refresh_token")
                .param("refresh_token", REFRESH_TOKEN)
                .header("Authorization", AUTHORIZATION_HEADER)
                .when().post("https://accounts.spotify.com/api/token").then().log()
                .ifValidationFails().statusCode(200).extract().response().getBody().path("access_token"));
    }


}
