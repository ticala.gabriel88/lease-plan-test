package com.lease.plan.utils;

public class TestConstants {

    public static final String CURRENT_USER_ID = "45nrfy9ayyanv89rfjrao3al2";
    public static final String TOP_50_GLOBAL_PLAYLIST_ID = "37i9dQZEVXbMDoHDwVN2tF";
    public static final String THE_WEEKND_AFTER_HOURS_ALBUM = "4yP0hdKOZPNshxUOjY0cZj";
    public static final String THE_WEEKND_ALONE_AGAIN_TRACK = "6b5P51m8xx2XA6U7sdNZ5E";
    public static final String JOE_ROGAN_MAT_FRASER_EPISODE = "7EMkEw8Rpg6vntJoKUfDk8";
    public static final String INVALID_PLAYLIST_ID = "37i9dQZEVXbMDoHDwVXXX";

    public static final String REFRESH_TOKEN = "AQCNmp2qKwUz8UUr1LiQMIH9Wvmjz2UhulNHxkJg5vlZfKXBS1l9U7doGuVyL8kbvi3VN9mwW-KNnOD4D5sOgpyhw_fcsefmTej8d1PeReuK-q7YdeNMC1rZbK3GZVbAu4w";
    public static final String AUTHORIZATION_HEADER = "Basic MWIyNzExMmMwMGIzNDU5NjlkZmExYjljY2I3MjQ2YzE6YTczZDRhN2ZlMTUxNDg5N2JlODYwYmFhYWQzMDc4NGI=";
    public static final String USER_PROFILE_ENDPOINT = "https://api.spotify.com/v1/users/";
    public static final String CURRENT_USERS_PLAYLIST_ENDPOINT = "https://api.spotify.com/v1/me/playlists";
    public static final String CURRENT_USERS_ALBUMS_ENDPOINT = "https://api.spotify.com/v1/me/albums";
    public static final String CURRENT_USERS_EPISODES_ENDPOINT = "https://api.spotify.com/v1/me/episodes";
    public static final String CURRENT_USERS_TRACKS_ENDPOINT = "https://api.spotify.com/v1/me/tracks";
    public static final String TOTAL = "total";
    public static final String VALID = "valid";
    public static final String INVALID_TOKEN = "INVALID_TOKEN";
    public static final int STATUS_CODE_200 = 200;
    public static final int STATUS_CODE_404 = 404;
    public static final int STATUS_CODE_400 = 400;
}
