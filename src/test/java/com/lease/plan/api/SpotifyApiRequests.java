package com.lease.plan.api;

import io.restassured.http.ContentType;

import static com.lease.plan.cucumber.steps.CucumberHooks.getAccessToken;
import static com.lease.plan.utils.TestConstants.CURRENT_USERS_ALBUMS_ENDPOINT;
import static com.lease.plan.utils.TestConstants.CURRENT_USERS_EPISODES_ENDPOINT;
import static com.lease.plan.utils.TestConstants.CURRENT_USERS_PLAYLIST_ENDPOINT;
import static com.lease.plan.utils.TestConstants.CURRENT_USERS_TRACKS_ENDPOINT;
import static com.lease.plan.utils.TestConstants.STATUS_CODE_200;
import static com.lease.plan.utils.TestConstants.STATUS_CODE_400;
import static com.lease.plan.utils.TestConstants.STATUS_CODE_404;
import static com.lease.plan.utils.TestConstants.TOTAL;
import static com.lease.plan.utils.TestConstants.VALID;
import static io.restassured.RestAssured.given;

public class SpotifyApiRequests {
    /**
     * Below method requests the number of tracks that the current user saved/follows
     * @return number of saved/followed tracks
     */
    public static int requestNumberOfCurrentUsersSavedTracks() {
        return given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .get(CURRENT_USERS_TRACKS_ENDPOINT)
                .then().log().ifValidationFails().statusCode(STATUS_CODE_200)
                .extract().response().getBody().path(TOTAL);
    }

    /**
     * Below method makes a request that requires 2 parameters, the track id that we want to be
     * saved to the current user and the data type, if it's invalid then we don't want to save
     * the track id to the user (to cover negative cases)
     * @param dataType type of data that we want to add: valid or invalid, which will result in different responses
     * @param trackID  id of the track you want to save
     */
    public static void saveTrack(String dataType, String trackID) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .put("https://api.spotify.com/v1/me/tracks?ids=" + trackID)
                .then().log().ifValidationFails().statusCode(VALID.equals(dataType) ? STATUS_CODE_200 : STATUS_CODE_400);
    }

    /**
     * Below method makes a request that requires 2 parameters, the episode id that we want to be
     * saved to the current user and the data type, if it's invalid then we don't want to save
     * the episode id to the user (to cover negative cases)
     * @param dataType  type of data that we want to add: valid or invalid, which will result in different responses
     * @param episodeID id of the episode you want to save
     */
    public static void saveEpisode(String dataType, String episodeID) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .put("https://api.spotify.com/v1/me/episodes?ids=" + episodeID)
                .then().log().ifValidationFails().statusCode(VALID.equals(dataType) ? STATUS_CODE_200 : STATUS_CODE_400);
    }

    /**
     * Below method makes a request that requires 2 parameters, the album id that we want to be
     * saved to the current user and the data type, if it's invalid then we don't want to save
     * the album id to the user (to cover negative cases)
     * @param dataType type of data that we want to add: valid or invalid, which will result in different responses
     * @param albumID  id of the album you want to save
     */
    public static void saveAlbum(String dataType, String albumID) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .put("https://api.spotify.com/v1/me/albums?ids=" + albumID)
                .then().log().ifValidationFails().statusCode(VALID.equals(dataType) ? STATUS_CODE_200 : STATUS_CODE_400);
    }

    /**
     * Below method makes a request that requires 2 parameters, the album id that we want to be
     * removed from the current user and the data type, if it's invalid then we don't want to remove
     * the album id to the user (to cover negative cases)
     * @param dataType type of data that we want to add: valid or invalid, which will result in different responses
     * @param albumID  id of the album you want to remove
     */
    public static void deleteAlbum(String dataType, String albumID) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .delete("https://api.spotify.com/v1/me/albums?ids=" + albumID)
                .then().log().ifValidationFails().statusCode(VALID.equals(dataType) ? STATUS_CODE_200 : STATUS_CODE_400);
    }

    /**
     * Below method makes a request that requires 2 parameters, the episode id that we want to be
     * removed from the current user and the data type, if it's invalid then we don't want to remove
     * the episode id to the user (to cover negative cases)
     * @param dataType type of data that we want to add: valid or invalid, which will result in different responses
     * @param episodeID  id of the episode you want to remove
     */
    public static void deleteEpisode(String dataType, String episodeID) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .delete("https://api.spotify.com/v1/me/episodes?ids=" + episodeID)
                .then().log().ifValidationFails().statusCode(VALID.equals(dataType) ? STATUS_CODE_200 : STATUS_CODE_400);
    }

    /**
     * Below method makes a request that requires 2 parameters, the track id that we want to be
     * removed from the current user and the data type, if it's invalid then we don't want to remove
     * the track id to the user (to cover negative cases)
     * @param dataType type of data that we want to add: valid or invalid, which will result in different responses
     * @param trackID  id of the track you want to remove
     */
    public static void deleteTrack(String dataType, String trackID) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .delete("https://api.spotify.com/v1/me/tracks?ids=" + trackID)
                .then().log().ifValidationFails().statusCode(VALID.equals(dataType) ? STATUS_CODE_200 : STATUS_CODE_400);
    }

    /**
     *  Below method makes a request that adds the playlistID into the follow list
     * @param playlistID id of the playlist that you want to follow
     * @param data type of data that we want to add: valid or invalid, which will result in different responses
     */
    public static void followPlaylistID(String playlistID, String data) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .put("https://api.spotify.com/v1/playlists/" + playlistID + "/followers")
                .then().log().ifValidationFails().statusCode(VALID.equals(data) ? STATUS_CODE_200 : STATUS_CODE_404);
    }

    /**
     *  Below method makes a request that removes the playlistID from the follow list
     * @param playlistID id of the playlist that you want to unfollow
     * @param dataType type of data that we want to add: valid or invalid, which will result in different responses
     */
    public static void unfollowPlaylistID(String playlistID, String dataType) {
        given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .delete("https://api.spotify.com/v1/playlists/" + playlistID + "/followers")
                .then().log().ifValidationFails().statusCode(VALID.equals(dataType) ? STATUS_CODE_200 : STATUS_CODE_404);
    }

    /**
     * Below method requests the number of playlists that the current user saved/follows
     * @return number of saved/followed playlists
     */
    public static int requestNumberOfCurrentUsersPlaylists() {
        return given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .get(CURRENT_USERS_PLAYLIST_ENDPOINT)
                .then().log().ifValidationFails().statusCode(STATUS_CODE_200)
                .extract().response().getBody().path(TOTAL);
    }

    /**
     * Below method requests the number of albums that the current user saved/follows
     * @return number of saved/followed albums
     */
    public static int requestNumberOfCurrentUsersSavedAlbums() {
        return given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .get(CURRENT_USERS_ALBUMS_ENDPOINT)
                .then().log().ifValidationFails().statusCode(STATUS_CODE_200)
                .extract().response().getBody().path(TOTAL);
    }

    /**
     * Below method requests the number of episodes that the current user saved/follows
     * @return number of saved/followed episodes
     */
    public static int requestNumberOfCurrentUsersSavedEpisodes() {
        return given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .get(CURRENT_USERS_EPISODES_ENDPOINT)
                .then().log().ifValidationFails().statusCode(STATUS_CODE_200)
                .extract().response().getBody().path(TOTAL);
    }

    /**
     * Below method checks whether the userId is following the playlistID
     * @param playlistID playlist that we want to check if it's save/followed
     * @param userId user that we want to check if it follows the playlist
     * @return true if the user is following the respective playlist, false if he doesn't
     */
    public static Boolean isUserIdFollowingPlaylistID(String playlistID, String userId) {
        return (Boolean) given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .get("https://api.spotify.com/v1/playlists/" + playlistID + "/followers/contains?ids=" + userId)
                .then().log().ifValidationFails().statusCode(STATUS_CODE_200).extract().response().jsonPath().getList(".").get(0);

    }

    /**
     * Below method checks whether the library contains the id of the track/episode/show/album
     * @param libraryType can be an album, track, episode, show
     * @param idToContain is the id of the track/episode/show/album that we want to check that is in the libraryType
     * @return true if the library contains the id entered, false otherwise
     */
    public static Boolean doesLibraryTypeContainID(String libraryType, String idToContain) {
        return (Boolean) given().accept(ContentType.JSON).contentType(ContentType.JSON).auth()
                .oauth2(getAccessToken())
                .get("https://api.spotify.com/v1/me/" + libraryType + "s/contains?ids=" + idToContain)
                .then().log().ifValidationFails().statusCode(STATUS_CODE_200).extract().response().getBody().jsonPath().getList(".").get(0);
    }
}
