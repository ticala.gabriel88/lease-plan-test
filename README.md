This test framework was created as a test assignment for Lease Plan as part of the interview process.

The requirements of the assignment can be found in the BE_QA_ASSIGNMENT.pdf

Prerequisites:
Java & Maven to be installed

### What technologies are used and what does this framework really test?

In this framework I used the following:

Junit, Serenity, Cucumber, REST-assured, SLF4J logger and maven failsafe. The language in which the tests were written
is Java and I used Maven for dependency management.

I used all these technologies in order to write API tests that verify the **Spotify API**

### How to run these tests?

The easiest way to run it is by command line: **mvn clean verify** which will run all the tests

If you want to run a specific test you can do by using this example:
**mvn clean verify -Dcucumber.filter.tags="@followNegativePath"** which runs the test with the mentioned tag

You can also run the tests by creating a Junit configuration in your favorite IDE and adding the TestRunner class into
it, from there if you go to the TestRunner class you can select which tags you want to run by changing the tag name
found in the tags cucumber option.

Another way to trigger all the tests is when you push code to a branch, the gitlab CI pipeline will be triggered and
your result can be viewed here: https://gitlab.com/ticala.gabriel88/lease-plan-test/-/pipelines

After each run you can see at the end in the console a link to HTML report with the results of the tests, that looks
like:

SERENITY REPORTS Full Report: file:///D:/lease-plan-test/target/site/serenity/index.html

### Writing new tests? Here's how to start

If you want to write a new feature file the feature files can be found in: src/test/resources/features

The Spotify API Requests are in the **com/lease/plan/api package** here you will find all the requests explained, the
methods were created in a way to be easy to be reused.

More details about the Spotify API can be found here: https://developer.spotify.com/console/

The step definitions are in **com/lease/plan/cucumber/steps/StepDefinitions**

The reusable data can be found in **com/lease/plan/utils/TestConstants**

**As a best practice please follow the same pattern as you can see in the project**

